<hr>

# Challenge Deep learning

<hr>

Etudiants ISIMA :
- Gladieux Cunha Dimitri
- Malka Laurent

Encadrants ISIMA :
- Christophe Tilmant
- Vincent Barra

## Technologies utilisées

Nous avons utilisé la librarie **Keras**

## Architecture

Vous trouverez dans ce compte rendu 
- **trainer.ipynb** : l'entrainement et la sauvegarde de notre réseau
- **prediction.ipynb** : Chargement de notre réseau et le test
- **model.h5** : Ce fichier est généré à la fin de **trainer.ipynb**
- **requirements.txt** : Issu de la commande `pip freeze > requirements.txt`

## Remarques

Nous n'avons pas trouvé de solution pour passer un paramètre pour le notebook **prediction.ipynb**. Par conséquent, vous trouverez en haut de ce fichier un paramètre appellé `absolute_path_images` qui vous permet de renseigner le chemin des images.  
Ex : `absolute_path_images = data/png-files/hand/`
(Pensez au `/` final)